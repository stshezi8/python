# print is a python function to display something here we are just printing string
print('Hello word')
# You can use the same function to Display calculation results
print(10 * 10)
# In this function we passed 2 Parameters separated by ","
print("15 * 4, The answer is ", 15 * 4)