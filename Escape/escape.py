splitString = "We can split strings using 'back slash n' like this Senzo\nTshezi \n this will start a new line  "
print(splitString)
tabbedString = "We can also put tab between string like this 1\t2\t3\t4\t5"
print(tabbedString)
print('In single quotes escape single quotes "Like this \'first\', \'Second\'.. end" ')
print("In Double Quotes escape Double Quotes 'Like this \"first\", \"Second\" .. end' ")
print("""Use Triple Quotes to escape Any Quotes used within these Triple Quotes "Like ths 'First', 'Second'..end ". """)

tripleSplitInMultipleLines = """This will be split in multiple lines
  First line 
  Second Line 
  Third Line """

print(tripleSplitInMultipleLines)

tripleRemoveSplitInMultipleLines = """This will be split in multiple lines \
  First line \
  Second Line \
  Third Line, We use back slash to escape end of the line  """

print(tripleRemoveSplitInMultipleLines)

# Backslashes
# Backslashes can be escaped by using 2 back slash
print("C:\\User\\tim\\note.txt")
# Or We can use raw string by juust puting r
print(r"C:\User\tim\note.txt")