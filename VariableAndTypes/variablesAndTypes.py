# NOTES
# Python variables can beginning with letters (Upper or Lower) or _under scores
# But they CAN’T beginning with a Number
# Python is case sensitive Greeting and greeting are not the same

name = "Senzo"
# In python we are not assigning we are binding ,
age = 24
print(name ) # string
print(age)# int

# But we can ask python what type is Name and Age
print(type(name))
print(type(age))

# you can also override the same variable from int to string in same code block , This is impossible in Java
# In Java we are Assigning , that makes difficult to assign int to string
age = "24 years"
print(age)