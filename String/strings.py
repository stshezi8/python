print("all in double quotes")
print('All in single quotes')
print("Double quotes and ' one single quote inside of double quotes ")
print('Single quote and "Double quotes" inside single quotes')
print("you can also concatenate strings " + "using plus sign")

# We can store strings in variables
greeting = "Hello"
name = "Senzo"
print (greeting + ' ' + name)

# We can also use the input to read Strings from keyboard
surname =input("Please enter your surname : ") # Input function
print(greeting + ' ' + name + ' ' + surname )